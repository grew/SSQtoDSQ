% ============================================================================================
package normalize_melt_output {
  rule fix_lemma {
    pattern { N [lemma = lex.in] }
    commands { N.lemma = lex.out}
  }
#BEGIN lex
in	out
%-------
*au	à
#END
}

% ============================================================================================
% Computation of deep lemmas for full verbs.
package verb_deep_lemma{
   rule dl_verb{
    pattern{V[cat=V,lemma,!dl]}
    without{V -[aff]-> *}
    without{V -[dep_cpd]-> *}
    without{* -[dep_cpd]-> V}
    commands{V.dl=V.lemma}}

  rule dl_verb-pron_aff{
    pattern{
      AFF[lemma <>"se"]; V -[aff]-> AFF;
      V[cat=V,lemma,!dl]}
    commands{V.dl=AFF.lemma + "_" +V.lemma}}

  rule dl_verb_se(lex from "lexicons/word_class/pronominal_verb.lp"){
    pattern{
      AFF[lemma ="se"]; V -[aff]-> AFF;
      V[cat=V,lemma=lex.verb,!dl]}
    commands{V.dl=AFF.lemma + "_" +V.lemma}}

  rule dl_verb-nonpron_se(lex from "lexicons/word_class/pronominal_verb.lp"){
    pattern{
      AFF[lemma ="se"];V -[aff]-> AFF;
      V[cat=V,lemma,!dl]}
    without{V[lemma=lex.verb]}
    commands{V.dl=V.lemma}}

  rule dl_verb_depcpd_init{
    pattern{
      V[cat=V,!dl];V -[dep_cpd]-> COMP}
    without{V -[dep_cpd]-> COMP1; COMP1 << COMP}
    without{V -[mark.tmp]-> *}
    commands{
      V.dl=V.lemma + "_" +COMP.lemma;
      add_edge V -[mark.tmp]-> COMP}}

  rule dl_verb_depcpd_propag{
    pattern{
      V[cat=V,dl];
       marktemp_rel:V -[mark.tmp]-> COMP0;
       V -[dep_cpd]-> COMP; COMP0 << COMP}
    without{V -[dep_cpd]-> COMP1; COMP1 << COMP}
    commands{
      V.dl=V.dl + "_" +COMP.lemma;
      del_edge marktemp_rel;
      add_edge V -[mark.tmp]-> COMP}}

  rule dl_verb_depcpd_finish{
    pattern{
      V[cat=V,dl];
       marktemp_rel:V -[mark.tmp]-> COMP}
    without{V -[dep_cpd]-> COMP1; COMP << COMP1}
    commands{del_edge marktemp_rel}}
}

% ============================================================================================
% Addition of deep moods for full verbs.
package  deep_verb_feat{

  rule auxcaus-tensed_inf{
    pattern{AUX[cat=V,m,t,!void]; V[cat=V,m,!dm,!dt]; V -[aux.caus]-> AUX}
    without{V -[aux.tps]-> *}
    commands{V.dm=AUX.m; V.dt=AUX.t}}

  rule auxcaus-inf_inf{
    pattern{AUX[cat=V,m=inf,!void]; V[cat=V,m,!dm,!dt];V -[aux.caus]-> AUX}
    commands{V.dm=inf;V.diat=caus}}

  rule auxpass-tensed_part{
    pattern{AUX[cat=V,m,t,!void]; V[cat=V,m=part,t=past,!dm,!dt]; V -[aux.pass]-> AUX}
    without{V -[aux.tps]-> *}
    commands{ V.dm=AUX.m; V.dt=AUX.t}}

  rule auxpass-inf_part{
    pattern{AUX[cat=V,m=inf,!void]; V[cat=V,m=part,t=past,!dm,!dt];V -[aux.pass]-> AUX}
    commands{V.dm=inf}}

  rule auxtps-tensed_inf{
    pattern{AUX[cat=V,m,t,!void]; V[cat=V,m,!dm,!dt];V -[aux.tps]-> AUX}
    commands{V.dm=AUX.m; V.dt=AUX.t}}

  rule auxtps-inf_inf{
    pattern{AUX[cat=V,m=inf,!void]; V[cat=V,m,!dm,!dt];V -[aux.tps]-> AUX}
    commands{V.dm=inf}}

 rule verb-full-tensed{
    pattern{V[cat=V,m,t,!dm,!dt]}
    without{V-[aux.caus|aux.pass|aux.tps]-> *}
    without{* -[aux.caus|aux.pass|aux.tps]-> V}
    without{* -[dep_cpd]-> V}
    commands{V.dm=V.m; V.dt=V.t}}

  rule verb-full-inf{
    pattern{V[cat=V,m=inf,!dm]}
    without{V-[aux.caus|aux.pass|aux.tps]-> *}
    without{* -[aux.caus|aux.pass|aux.tps]-> V}
    without{* -[dep_cpd]-> V}
    commands{V.dm=inf}}

  rule coord_compound_tensed {
    pattern {
      V1 [m=part,t=past,dm <> part, dt]; V2 [cat=V,dm=part,dt=past];
      V1 -[coord]-> C; C -[dep.coord]-> V2}
    commands {V2.dm = V1.dm; V2.dt = V1.dt }}

  rule coord_compound_inf {
    pattern {
      V1 [m=part,t=past,dm=inf]; V2 [cat=V,dm=part,dt=past];
      V1 -[coord]-> C; C -[dep.coord]-> V2}
    commands { V2.dm = inf}}

  rule coord_causative_tensed{
    pattern {
      V1 [m=inf,dm,dt,diat=caus]; V2 [cat=V,m=inf,dm=inf];
      V1 -[coord]-> C; C -[dep.coord]-> V2}
    commands {V2.dm = V1.dm; V2.dt = V1.dt;V2.diat=caus}}

  rule coord_causative_inf{
    pattern {
      V1 [m=inf,dm=inf,dt,diat=caus]; V2 [cat=V,m=inf,dm=inf];
      V1 -[coord]-> C; C -[dep.coord]-> V2}
    commands {V2.dm = inf; V2.diat=caus}}

}

% ============================================================================================
% Marking verbs as transitive or not.
package  trans_verb{

  rule transitive(lex from "lexicons/word_class/transitive_verb.lp"){
    pattern{V[cat=V,lemma=lex.verb, !trans]}
    without{V -[aff]-> *}
    commands{V.trans=y}}

  rule intransitive(lex from "lexicons/word_class/transitive_verb.lp"){
    pattern{V[cat=V,!trans]}
    without{V[lemma=lex.verb]}
    commands{V.trans=n}}

}

% ============================================================================================
% Typing clauses as interrogative or exclamative.
package cl_type{
  rule inter{
    pattern{PUNCT[cat=PONCT, lemma="?"]; HEAD -> PUNCT}
    without { HEAD[cltype=int] }
    commands { HEAD.cltype=int }}

  rule excl{
    pattern{PUNCT[cat=PONCT, lemma="!"]; HEAD[!cltype];HEAD -> PUNCT}
    without{HEAD[cat=V,dm=imp]}
    commands { HEAD.cltype=excl }}

  rule imp{
    pattern{
      PUNCT[cat=PONCT, lemma="!"];
      HEAD[cat=V,dm=imp,!cltype];HEAD -> PUNCT}
    commands { HEAD.cltype=imp }}

  rule inter-si {
    pattern{
      SI[pos=CS,lemma="si"];
      V[cat=V]; SI -[obj.cpl]-> V; MAINV -[obj]-> SI }
    without{V[cltype=int] }
    commands { V.cltype=int }}

  rule inter-wh-direct {
    pattern{ V[cat=V]; WH[pos=ADVWH|PROWH]; V -> WH }
    without {V[cltype=int] }
    commands {V.cltype=int }}

  rule inter-wh-indirect1 {
    pattern{
      V[cat=V]; V -> W; W[cat=P|N];
      WH[pos=DETWH|PROWH]; W -> WH }
    without {V[cltype=int] }
    commands {V.cltype=int }}

  rule inter-wh-indirect12 {
    pattern{
      V[cat=V]; P[cat=P]; V -> P;
      P -[obj.p]-> W;
      WH[pos=DETWH]; W -> WH }
    without {V[cltype=int] }
    commands {V.cltype=int }}
}
% ============================================================================================
% When there is a repeated subject, the clitic subject is deleted.
package repeated_subject {
  rule del_suj {
    pattern {
      V[cat=V];
      SUBJ [cat<>CL]; V -[suj|D:suj]-> SUBJ;
      CLIT [cat=CL];  V -[suj]-> CLIT}
    without { CLIT [void=y] }
    commands { CLIT.void=y }}

}

% ============================================================================================
% Concatenate the lemma of light verbs with the lemma of their object
package light_verb {

% Ex:   Max a envie de partir.
  rule verb-light_obj (lex from "lexicons/subcat/verb-light_obj_noun.lp") {
    pattern {
      V[cat=V,lemma=lex.verb,!initlemma];
      N [cat=N, lemma=lex.noun ]; V -[obj]-> N} % le nom prédicatif
    commands {V.initlemma=V.lemma; V.lemma = V.lemma + "_" + N.lemma }}

% Ex:  Europar.550_00273 - il n'y a pas lieu de noter ....
  rule verb-light-aff_obj {
    pattern {
      V[cat=V,lemma=lex.verb,!initlemma];
      AFF[cat=CL,lemma=lex.aff]; V -[aff]-> AFF;
      N [cat=N, lemma=lex.noun ]; V -[obj]-> N} % le nom prédicatif
    commands {V.initlemma=V.lemma; V.lemma = V.lemma + "_" + N.lemma }}
#BEGIN lex
aff	verb	noun
%--------------
y	avoir	lieu
#END


% Ex:   Max est en droit de demander un remboursement.
  rule verb-light_pobjo {
    pattern {
      V[cat=V,lemma=lex.verb,!initlemma];
      PREP[cat=P, lemma=lex.prep]; V -[p_obj.o]-> PREP;
      N[cat=N, lemma=lex.noun]; PREP -[obj.p]-> N}
    commands {V.initlemma=V.lemma; V.lemma = V.lemma + "_" +PREP.lemma + "_" + N.lemma }
  }
#BEGIN lex
verb	prep	noun
%---------------
être	en	droit
#END

}

% =============================================================================================
% Unfolding Sequoia phonological contractions.
package phon_unfold{

% A preposition is contracted with a determiner in singular and the first dependency on its right is an OBJ.P dependency.
  rule prep-det-sg_objp (lex from "lexicons/word_class/sg-contraction-decomp.lp") {
    pattern{ PREP[cat="P+D",phon=lex.amalgam]; PREP -[obj.p]-> OBJ}
    without{ PREP -[obj.p|dep_cpd]-> OBJ1; PREP << OBJ1; OBJ1 << OBJ}
    commands{
      add_node DET:> PREP; add_edge OBJ -[det]-> DET;
      del_feat PREP.s; DET.s=def;
      DET.phon=lex.det_form; DET.lemma=lex.det_lemma;DET.cat=D; DET.g=m; DET.n=s;
      PREP.phon=lex.prep_form; PREP.lemma=lex.prep_lemma;
      PREP.cat=P;PREP.pos=P}}

% A preposition is contracted with a determiner in singular and the first dependency on its right is a DEP_CPD dependency.
  rule prep-det-sg_depcpd (lex from "lexicons/word_class/sg-contraction-decomp.lp") {
    pattern{ PREP[cat="P+D",phon=lex.amalgam,s]; PREP -[dep_cpd]-> OBJ}
    without{ PREP -[obj.p|dep_cpd]-> OBJ1; PREP << OBJ1; OBJ1 << OBJ}
    commands{
      add_node DET:> PREP; add_edge PREP -[dep_cpd]-> DET;
      del_feat PREP.s; DET.s=def;
      DET.phon=lex.det_form; DET.lemma=lex.det_lemma;DET.cat=D; DET.g=m; DET.n=s;
      PREP.phon=lex.prep_form; PREP.lemma=lex.prep_lemma;
      PREP.cat=P;PREP.pos=P}}

% A preposition is contracted with a determiner in plural and the first dependency on its right is an OBJ.P dependency.
  rule prep-det-pl_objp (lex from "lexicons/word_class/pl-contraction-decomp.lp") {
    pattern{ PREP[cat="P+D",phon=lex.amalgam]; PREP -[obj.p]-> OBJ}
    without{ PREP -[obj.p|dep_cpd]-> OBJ1;PREP << OBJ1; OBJ1 << OBJ}
    commands{
      add_node DET:>PREP; add_edge OBJ -[det]-> DET;
      del_feat PREP.s; DET.s=def;
      DET.phon=lex.det_form; DET.lemma=lex.det_lemma;DET.cat=D;  DET.n=p;
      PREP.phon=lex.prep_form; PREP.lemma=lex.prep_lemma;
      PREP.cat=P;PREP.pos=P}}

% A preposition is contracted with a determiner in plural and the first dependency on its right is a DEP_CPD dependency.
  rule prep-det-pl_depcpd (lex from "lexicons/word_class/pl-contraction-decomp.lp") {
    pattern{ PREP[cat="P+D",phon=lex.amalgam]; PREP -[dep_cpd]-> OBJ}
    without{ PREP -[obj.p|dep_cpd]-> OBJ1;PREP << OBJ1; OBJ1 << OBJ}
    commands{
      add_node DET:>PREP; add_edge OBJ -[det]-> DET;
      del_feat PREP.s; DET.s=def;
      DET.phon=lex.det_form; DET.lemma=lex.det_lemma;DET.cat=D;  DET.n=p;
      PREP.phon=lex.prep_form; PREP.lemma=lex.prep_lemma;
      PREP.cat=P;PREP.pos=P}}

% If a preposition is contracted with a relative pronoun, they are dissociated.
%  rule prep-pro(feature $phon, @prepphon, @preplemma, @prophon, @prolemma, @num, @gen) {
%    pattern{ PREP[cat="P+PRO",phon=lex.amalgam]}
%    commands{
%      add_node PRO:>PREP; add_edge PREP -[obj.p]-> PRO;
%      PRO.phon=@prophon; PRO.lemma=@prolemma;PRO.cat=PRO;
%      del_feat PREP.s; PRO.s=def;
%      PRO.pos=PROREL;PRO.g=@gen; PRO.n=@num;
%      PREP.phon=lex.prep_form; PREP.lemma=lex.prep_lemma;
%      PREP.initcat="P+PRO";PREP.initpos="P+PRO";PREP.cat=P;PREP.pos=P}}
%#BEGIN
%auquel##à#à#lequel#lequel#p#m
%Auquel##À#à#lequel#lequel#p#m
%auxquels##à#à#lesquels#lequel#p#m
%auxquelles##à#à#lesquelles#lequel#p#f
%duquel##de#de#lequel#lequel#s#m
%desquels##de#de#lesquels#lequel#s#m
%desquelles##de#de#lesquelles#lequel#s#m
%#END

% There is DEP_CPD dependency to a preposition contracted with a determiner in singular and the first dependency on the right is an OBJ.P dependency.
  rule depcpd_prep-det-sg_objp (lex from "lexicons/word_class/sg-contraction-decomp.lp") {
    pattern{
      HEAD -[dep_cpd]-> PREP; PREP[cat="P+D",phon=lex.amalgam];
      HEAD -[obj.p]-> DEP; PREP << DEP}
    without {HEAD -> X; PREP << X ; X << DEP }
    commands{
      add_node DET:> PREP; add_edge DEP -[det]-> DET;
      del_feat PREP.s; DET.s=def;
      DET.phon=lex.det_form; DET.lemma=lex.det_lemma;DET.cat=D; DET.g=m; DET.n=s;
      PREP.phon=lex.prep_form; PREP.lemma=lex.prep_lemma;
      PREP.cat=P;PREP.pos=P}}

% There is DEP_CPD dependency to a preposition contracted with a determiner in singular and the first dependency on the right is a DEP_CPD dependency.
  rule depcpd_prep-det-sg_depcpd (lex from "lexicons/word_class/sg-contraction-decomp.lp") {
    pattern{
      HEAD -[dep_cpd]-> PREP; PREP[cat="P+D",phon=lex.amalgam];
      HEAD -[dep_cpd]-> DEP; PREP << DEP}
    without {HEAD -> X; PREP << X ; X << DEP }
    commands{
      add_node DET:> PREP; add_edge HEAD -[dep_cpd]-> DET;
      del_feat PREP.s; DET.s=def;
      DET.phon=lex.det_form; DET.lemma=lex.det_lemma;DET.cat=D; DET.g=m; DET.n=s;
      PREP.phon=lex.prep_form; PREP.lemma=lex.prep_lemma;
      PREP.cat=P;PREP.pos=P}}

% There is DEP_CPD dependency to a prepostion contracted with a determiner in plural and the first dependency on the right is an OBJ.P dependency.
  rule depcpd_prep-det-pl_objp (lex from "lexicons/word_class/pl-contraction-decomp.lp") {
  pattern{
    HEAD -[dep_cpd]-> PREP; PREP[cat="P+D",phon=lex.amalgam];
    HEAD -[obj.p]-> DEP ; PREP << DEP}
  without {HEAD -> X;  PREP << X ; X << DEP}
  commands{
    add_node DET:> PREP; add_edge DEP -[det]-> DET;
    del_feat PREP.s; DET.s=def;
    DET.phon=lex.det_form; DET.lemma=lex.det_lemma;DET.cat=D; DET.g=m; DET.n=s;
    PREP.phon=lex.prep_form; PREP.lemma=lex.prep_lemma;
    PREP.cat=P;PREP.pos=P}}

% There is DEP_CPD dependency to a prepostion contracted with a determiner in plural and the first dependency on the right is a DEP_CPD dependency.
  rule depcpd_prep-det-pl_depcpd (lex from "lexicons/word_class/pl-contraction-decomp.lp") {
  pattern{
    HEAD -[dep_cpd]-> PREP; PREP[cat="P+D",phon=lex.amalgam];
    HEAD -[dep_cpd]-> DEP ; PREP << DEP}
  without {HEAD -> X;  PREP << X ; X << DEP}
  commands{
    add_node DET:> PREP; add_edge HEAD -[dep_cpd]-> DET;
    del_feat PREP.s; DET.s=def;
    DET.phon=lex.det_form; DET.lemma=lex.det_lemma;DET.cat=D; DET.g=m; DET.n=s;
    PREP.phon=lex.prep_form; PREP.lemma=lex.prep_lemma;
    PREP.cat=P;PREP.pos=P}}
}

% ============================================================================================
% Add the feature def=y to all definite nouns.
package def_noun {
  rule det{
    pattern { N[cat=N]; D[s=def]; N -[det]-> D}
    without { N [def=y] }
    commands { N.def=y }}

  rule coord {
    pattern { N1[cat=N, def=y]; C[cat=C]; N2 [cat=N]; N1 -[coord]-> C; C -[dep.coord]-> N2 }
    without { N2 [def=y] }
    without { N2 -[det]-> * } % only if N2 does not have its own determiner
    commands { N2.def=y }}

}
% ===========================================================================================
package promote_mwe {
  % TODO: replace 10 rules by 1 lexical rule
  rule adv{
    pattern{N[mwehead="ADV",!initcat,!initpos]}
    commands {
      N.initcat=N.cat; N.cat=ADV;
      N.initpos=N.pos; N.pos=ADV}}

  rule adj{
    pattern{N [mwehead="ADJ",!initcat,!initpos] }
    commands {
      N.initcat=N.cat; N.cat=A;
      N.initpos=N.pos; N.pos=ADJ}}

  rule cc{
    pattern{N [mwehead="CC",!initcat,!initpos] }
    commands{
      N.initcat=N.cat; N.cat=C;
      N.initpos=N.pos; N.pos=CC}}

  rule cs{
    pattern{N[mwehead="CS",!initcat,!initpos]}
    commands{
      N.initcat=N.cat; N.cat=C;
      N.initpos=N.pos; N.pos=CS}}

  rule det{
    pattern{N[mwehead="DET",!initcat,!initpos]}
    commands{
      N.initcat=N.cat; N.cat=D;
      N.initpos=N.pos; N.pos=DET}}

  rule noun-common {
    pattern { N [mwehead="NC",!initcat,!initpos] }
    commands {
      N.initcat=N.cat; N.cat=N;
      N.initpos=N.pos; N.pos=NC}}

  rule noun-proper {
    pattern { N [mwehead="NPP",!initcat,!initpos] }
    commands {
      N.initcat=N.cat; N.cat=N;
      N.initpos=N.pos; N.pos=NPP}}

  rule prep{
    pattern{N[mwehead="P",!initcat,!initpos] }
    commands{
      N.initcat=N.cat; N.cat=P;
      N.initpos=N.pos; N.pos=P}}

  rule prep_det{
    pattern{N[mwehead="P+D",!initcat,!initpos] }
    commands{
      N.initcat=N.cat; N.cat="P+D";
      N.initpos=N.pos; N.pos="P+D"}}

  rule pro{
    pattern{N[mwehead="PRO",!initcat,!initpos]}
    commands{
      N.initcat=N.cat; N.cat=PRO;
      N.initpos=N.pos; N.pos=PRO}}
}


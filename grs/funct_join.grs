% ============================================================================================
% Merging final surface dependencies linked with canonical dependencies
package surf_final_can_join{

  rule aobj_suj{
    pattern{ final_rel: X -[a_obj]-> Y; can_rel: X -[C:suj]-> Y}
    commands{
      del_edge final_rel;  del_edge can_rel;
      add_edge X -[a_obj:suj]-> Y}}

  rule ats_ato{
    pattern{ final_rel: X -[ats]-> Y; can_rel: X -[C:ato]-> Y}
    commands{
      del_edge final_rel;  del_edge can_rel;
      add_edge X -[ats:ato]-> Y}}

  rule dis_suj{
    pattern{ final_rel: X -[dis]-> Y; can_rel: X -[C:suj]-> Y}
    commands{
      del_edge final_rel;  del_edge can_rel;
      add_edge X -[dis:suj]-> Y}}

  rule obj_obj{
    pattern{ final_rel: X -[obj]-> Y; can_rel: X -[C:obj]-> Y}
    commands{
      del_edge final_rel;  del_edge can_rel;
      add_edge X -[obj:obj]-> Y}}

  rule obj_suj{
    pattern{ final_rel: X -[obj]-> Y; can_rel: X -[C:suj]-> Y}
    commands{
      del_edge final_rel;  del_edge can_rel;
      add_edge X -[obj:suj]-> Y}}

  rule pobjagt_suj{
    pattern{ final_rel: X -[p_obj.agt]-> Y; can_rel: X -[C:suj]-> Y}
    commands{
      del_edge final_rel;  del_edge can_rel;
      add_edge X -[p_obj.agt:suj]-> Y}}

  rule suj_argc{
    pattern{ final_rel: X -[suj]-> Y; can_rel: X -[C:argc]-> Y}
    commands{
      del_edge final_rel;  del_edge can_rel;
      add_edge X -[suj:argc]-> Y}}

  rule suj_obj{
    pattern{ final_rel: X -[suj]-> Y; can_rel: X -[C:obj]-> Y}
    commands{
      del_edge final_rel;  del_edge can_rel;
      add_edge X -[suj:obj]-> Y}}

  rule suj_void{
    pattern{ final_rel: X -[suj]-> Y; can_rel: X -[C:void]-> Y}
    commands{
      del_edge final_rel;  del_edge can_rel;
      add_edge X -[S:suj:_]-> Y; Y.void=y}}
}

% ============================================================================================
% Identical final and canonical functions for surface dependencies without diathesis change.
package deflt_surf_final_can_join{

  rule ato{
    pattern{ final_rel: X -[ato]-> Y}
    commands{del_edge final_rel; add_edge X -[ato:ato]-> Y}}

  rule ats{
    pattern{ final_rel: X -[ats]-> Y}
    commands{del_edge final_rel; add_edge X -[ats:ats]-> Y}}

  rule aobj{
    pattern{ final_rel: X -[a_obj]-> Y}
    commands{del_edge final_rel; add_edge X -[a_obj:a_obj]-> Y}}

  rule deobj{
    pattern{ final_rel: X -[de_obj]-> Y}
    commands{del_edge final_rel; add_edge X -[de_obj:de_obj]-> Y}}

  rule obj{
    pattern{ final_rel: X -[obj]-> Y}
    commands{del_edge final_rel; add_edge X -[obj:obj]-> Y}}

  rule obj-surf{
    pattern{ final_rel: X -[S:obj]-> Y}
    commands{del_edge final_rel; add_edge X -[S:obj:obj]-> Y}}

  rule suj{
    pattern{ final_rel: X -[suj]-> Y}
    commands{del_edge final_rel; add_edge X -[suj:suj]-> Y}}
}

% ============================================================================================
% Merging final deep dependencies linked with canonical dependencies
package deep_final_can_join{

  rule aobj_suj{
    pattern{ final_rel: X -[D:a_obj]-> Y; can_rel: X -[C:suj]-> Y}
    commands{
      del_edge final_rel;  del_edge can_rel;
      add_edge X -[D:a_obj:suj]-> Y}}

  rule pobjagt_suj{
    pattern{ final_rel: X -[D:p_obj.agt]-> Y; can_rel: X -[C:suj]-> Y}
    commands{
      del_edge final_rel;  del_edge can_rel;
      add_edge X -[D:p_obj.agt:suj]-> Y}}

  rule suj_argc{
    pattern{ final_rel: X -[D:suj]-> Y; can_rel: X -[C:argc]-> Y}
    commands{
      del_edge final_rel;  del_edge can_rel;
      add_edge X -[D:suj:argc]-> Y}}

  rule suj_obj{
    pattern{ final_rel: X -[D:suj]-> Y; can_rel: X -[C:obj]-> Y}
    commands{
      del_edge final_rel;  del_edge can_rel;
      add_edge X -[D:suj:obj]-> Y}}

}

% ============================================================================================
% Identical final and canonical functions for deep dependencies without diathesis change.
package deflt_deep_final_can_join{

  rule ato{
    pattern{ final_rel: X -[D:ato]-> Y}
    commands{del_edge final_rel; add_edge X -[D:ato:ato]-> Y}}

  rule ats{
    pattern{ final_rel: X -[D:ats]-> Y}
    commands{del_edge final_rel; add_edge X -[D:ats:ats]-> Y}}

  rule aobj{
    pattern{ final_rel: X -[D:a_obj]-> Y}
    commands{del_edge final_rel; add_edge X -[D:a_obj:a_obj]-> Y}}

  rule deobj{
    pattern{ final_rel: X -[D:de_obj]-> Y}
    commands{del_edge final_rel; add_edge X -[D:de_obj:de_obj]-> Y}}

  rule obj{
    pattern{ final_rel: X -[D:obj]-> Y}
    commands{del_edge final_rel; add_edge X -[D:obj:obj]-> Y}}

  rule suj{
    pattern{ final_rel: X -[D:suj]-> Y}
    commands{del_edge final_rel; add_edge X -[D:suj:suj]-> Y}}
}

#GREW = grew
GREW = grew_dev

selfdoc:
	@echo "demo    : open GUI 'E475"
	@echo "full    : rewrite full deep-sequoia_surf"
	@echo "score   : compute score for different labels"
	@echo "purge   : remove all generated files"

full: _auto.conll

score: _auto.conll
	attachment_score _auto.conll sequoia.deep_and_surf.conll

_auto.conll: sequoia.surf.conll
	${GREW} transform -grs grs/main_dsq.grs -i $< -o $@

demo: _Europar.550_00475.conll
	${GREW} gui -grs grs/main_dsq.grs -i $<

one:
	${GREW} gui -grs grs/main_dsq.grs -i one.conll

gui:
	${GREW} gui -grs grs/main_dsq.grs -i sequoia.surf.conll

_Europar.550_00475.conll: sequoia.surf.conll
	sed -n 50033,50080p $< > $@


demodebug: _Europar.550_00475.conll
	${GREW} gui -grs grs/main_dsq.grs -i $< -debug

compare: _auto.conll
	splitter _auto.conll _auto
	splitter sequoia.deep_and_surf.conll _gold
	dep_diff -d1 _gold -d2 _auto -svg _svg -html _diff.html -short

purge:
	rm -rf _*

# Download links
_sequoia.deep_and_surf.conll:
	wget "https://gitlab.inria.fr/sequoia/deep-sequoia/raw/master/trunk/sequoia.deep_and_surf.conll" -O $@

_sequoia.surf.conll:
	wget "https://gitlab.inria.fr/sequoia/deep-sequoia/raw/master/trunk/sequoia.surf.conll" -O $@
